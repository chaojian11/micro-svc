package com.test.auth.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;
import com.test.auth.mapper.LoginMapper;
import com.test.auth.service.LoginService;
import com.test.common.constant.Constant;
import com.test.common.enumtype.FwWebError;
import com.test.common.exception.ResultReturnException;
import com.test.common.model.Result;
import com.test.common.util.AesCbcUtil;
import com.test.common.util.DateUtil;
import com.test.common.util.HttpClientUtil;
import com.test.common.util.MD5Util;
import com.test.shiro.entity.UserEntity;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@Service
public class LoginServiceImpl implements LoginService{
	
	@Autowired
	private LoginMapper loginMapper;

	@Override
	public UserEntity byLoginName(String loginName, String password) {
		
		UserEntity userEntity = loginMapper.queryUserByLoginName(loginName);
		if (null == userEntity || userEntity.getId() <= 0) {
			throw new ResultReturnException(FwWebError.WRONG_ACCOUNT_OR_PSW);
		}
		// 先退出， 再登录
		SecurityUtils.getSubject().logout();
		
		return this.doLogin(userEntity, password);
	}
	
	
	/**
	 * 用户登录，公共方法
	 * @param UserEntity 登录查询到的用户
	 * @param password 用户输入的密码，可以为空
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public UserEntity doLogin(UserEntity userEntity, String password) {
		if (null == userEntity || userEntity.getId() <= 0) {
			throw new ResultReturnException(FwWebError.WRONG_ACCOUNT_OR_PSW);
		}
		
		UserEntity vo = new UserEntity();
		vo.setId(userEntity.getId());
		// 锁定半个小时后, 重新登录自动解锁
		if (DateUtil.getDatePoor(new Date(), userEntity.getUpdateTime()) >= 30) {
			vo.setUserStatus((short) 1);
			vo.setFailCount((short) 0);
			if (loginMapper.updateByPrimaryKeySelective(vo) > 0) {
				userEntity.setUserStatus((short) 1);
				userEntity.setFailCount((short) 0);
			}
		}
		if (userEntity.getUserStatus() == 2) {
			throw new ResultReturnException("您的账号已被停用");
		}
		if (userEntity.getUserStatus() == 3) {
			throw new ResultReturnException("账号已被锁定，请半小时后重新登录，或者联系管理员处理");
		}

		if (userEntity.getFailCount() > 5) {
			vo.setUserStatus((short) 3);
			loginMapper.updateByPrimaryKeySelective(vo);
			throw new ResultReturnException("密码连续错误5次，账号已被锁定，请半小时后重新登录，或者联系管理员处理");
		}

		if (!StringUtils.isEmpty(password)) {
			String passwd = this.getPassword(userEntity.getSalt(), password);
			if (!userEntity.getLoginPasswd().equals(passwd)) {
				// 更新failcount + 1
				vo.setFailCount((short) (userEntity.getFailCount() + 1));
				loginMapper.updateByPrimaryKeySelective(vo);
				throw new ResultReturnException("用户名或者密码有误，错误次数：" + vo.getFailCount());
			}
			userEntity.setLoginPasswd(passwd);
		}
		
		// 设置用户部门信息
		userEntity.setDeptIds(null);
		
		// 获取角色id
		userEntity.setRoleIds(null);
		
		// 获取用户权限，放到redis缓存
		Set<Integer> permsSet = Sets.newHashSet();
		if ("admin".equals(userEntity.getLoginName())) {
			permsSet = loginMapper.queryPermissionByAdmin();
		} else {
			permsSet = loginMapper.queryPermissionByUser(userEntity.getId());	// 加载所有权限
		}
		userEntity.setPermissionId(permsSet);
		
		this.shiroLogin(userEntity);
		
		vo.setFailCount((short) 0);	// 登录成功，更新failcount重置为0、 最后登录时间
		loginMapper.updateByPrimaryKeySelective(vo);

		userEntity.setLoginPasswd(null);
		return userEntity;
	}

	/**
	 * 组装token，登录，将用户信息缓存到redis
	 * @param user
	 */
	private void shiroLogin(UserEntity user) {
		user.setSalt(null);
		// 组装token
		UsernamePasswordToken token = new UsernamePasswordToken(user.getLoginName(), user.getLoginPasswd().toCharArray());
		Session session = SecurityUtils.getSubject().getSession();
		user.setToken(session.getId().toString());
		session.setAttribute(Constant.SSO_USER, user);
		// shiro登陆验证
		SecurityUtils.getSubject().login(token);
	}
	
	
	public String getPassword(String salt, String password) {
		// 用户登录密码的key
		String input = salt + MD5Util.encrypt32(password) + "73a526076a830e445905d596157729bf";
		return MD5Util.encrypt32(input);
	}
	
	
	private JSONObject getWechatSession(String appid, String code) {
		String wechatkey = "申请微信小程序应用获取到的key";
		String wechatsercet = "申请微信小程序应用获取到的sercet";
		Map<String, String> paramMap = new HashMap<String, String>();

		// 授权（必填）
		paramMap.put("appid", wechatkey);
		paramMap.put("secret", wechatsercet);
		paramMap.put("js_code", code);
		String grantType = "authorization_code";
		paramMap.put("grant_type", grantType);

		// 发送请求
		String sr = HttpClientUtil.post(Constant.WECHAT_REQ_URL, paramMap, null);

		// 解析相应内容（转换成json对象）
		return JSONObject.parseObject(sr);
	}

	// 需要调用统一认证的redis sys setting数据
	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object loginByMiniPro(Map<String, String> paramMap) {
		// 解析相应内容（转换成json对象）
		JSONObject json = this.getWechatSession(paramMap.get("appid"), paramMap.get("js_code"));
		// 获取会话密钥（session_key）
		String sessionKey = json.getString("session_key");
		// 微信登录异常，，返回异常信息 --验证是否微信登录
		if (StringUtils.isEmpty(sessionKey)) {
			return Result.error("微信登录异常");
		}
		// 用户的唯一标识（openid）
		String openId = json.getString("openid");

		// 微信登录成功，验证是否已经绑定设施系统账号
		// 根据openId查询用户信息，调用shiro登陆，返回session
		UserEntity user = loginMapper.queryUserByOpenId(openId);

		// 更新用户微信信息
		try {
			String result = AesCbcUtil.decrypt(paramMap.get("encryptedData"), sessionKey, paramMap.get("iv"), "UTF-8");
			if (!StringUtils.isEmpty(result)) {
				JSONObject userInfoJSON = JSONObject.parseObject(result);
				if (null == user || user.getId() <= 0) {
					loginMapper.insertWechatUser(userInfoJSON);
				} else {
					// 更新用户最新的微信个人信息
					loginMapper.updateWechatUser(userInfoJSON);
				}
			}
		} catch (Exception e) {
		}
		if (null == user || user.getId() <= 0) {
			return Result.error("未绑定系统账号");
		}
		return this.doLogin(user, null);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object bindUserByMiniPro(Map<String, String> paramMap) {
		String appid = paramMap.get("appid");
		String code = paramMap.get("code");
		String loginName = paramMap.get("loginName");
		String loginPasswd = paramMap.get("loginPasswd");

		// 解析相应内容（转换成json对象）
		JSONObject json = this.getWechatSession(appid, code);
		// 获取会话密钥（session_key）
		String sessionKey = json.getString("session_key");
		// 微信登录异常，，返回异常信息 --验证是否微信登录
		if (StringUtils.isEmpty(sessionKey)) {
			return Result.error("微信登录异常");
		}

		// 用户的唯一标识（openid）
		String openId = json.getString("openid");

		return this.bindUserAndLogin(appid, openId, loginName, loginPasswd, 0);
	}


	@Override
	public void loginByPublicNo(Map<String, String> paramMap, HttpServletRequest request, HttpServletResponse response) throws IOException {
		paramMap.put("register_url", "检测到未登录，重定向的主页页面路径url");
		// 获取微信登录token
		paramMap.put("appid", "申请微信公众号服务获取到的appid");
		paramMap.put("secret", "申请微信公众号服务获取到的secret");
		paramMap.put("grant_type", "authorization_code");
		String result = HttpClientUtil.doGet("https://api.weixin.qq.com/sns/oauth2/access_token", paramMap);
		JSONObject jsonObject = JSONObject.parseObject(result);
		String openId = jsonObject.getString("openid");
		if (StringUtils.isEmpty(openId)) {
			response.sendRedirect(paramMap.get("register_url"));
		}

		UserEntity systemUser = loginMapper.queryUserByOpenId(openId);
		if (systemUser == null) {
			// 发送请求，获取微信用户信息
			Map<String, String> paramsMap = new HashMap<>();
			paramsMap.put("access_token", jsonObject.getString("access_token"));
			paramsMap.put("openid", openId);

			String str = HttpClientUtil.doGet("https://api.weixin.qq.com/sns/userinfo", paramsMap);
			jsonObject = JSONObject.parseObject(str);

			loginMapper.deleteWechatUser(openId);
			// 保存公众号微信用户信息
			jsonObject.put("gender", jsonObject.getShort("sex"));
			jsonObject.put("avatarUrl", jsonObject.getString("headimgurl"));
			jsonObject.put("nickName", jsonObject.getString("nickname"));
			jsonObject.put("openId", openId);
			loginMapper.insertWechatUser(jsonObject);

			// 将openid放入session缓存，绑定的时候需要用到
			request.getSession().setAttribute("openId", openId);
			// 未绑定用户，由前端负责跳转到绑定页面
			// return ResponseResult.failed("当前微信未绑定系统用户");
			response.sendRedirect(paramMap.get("register_url") + "?" + Constant.REDIRECT_URL + "=" + paramMap.get("success_url"));
		} else {
			this.doLogin(systemUser, null);
			Session session = SecurityUtils.getSubject().getSession();

			String url = paramMap.get("success_url").toString();
			if (url.indexOf("?") > -1) {
				url = url + "&" + Constant.TOKEN + "=" + session.getId();
				// 特殊处理 测试发现微信截掉了部分参数
				url = url.replace("~", "&");
			} else {
				url = url + "?" + Constant.TOKEN + "=" + session.getId();
			}
			response.sendRedirect(url);
		}
	}

	
	@Override
	public Object bindUserAndLogin(String appid, String openId, String loginName, String password, Integer wechatType) {
		JSONObject params = new JSONObject();
		params.put("openId", openId);
		int result = loginMapper.judgeWeChatIsBind(params);
		if (result > 0) {
			return Result.error("该微信号已经绑定账号! ");
		}

		UserEntity record = new UserEntity();
		record.setLoginName(loginName);
		UserEntity systemUser = loginMapper.findByCondition(record);
		if (systemUser == null || systemUser.getId() <= 0) {
			return Result.error("账号不存在！");
		}

		// 校验密码是否正确
		String passwd = this.getPassword(systemUser.getSalt(), password);
		if (!systemUser.getLoginPasswd().equals(passwd)) {
			return Result.error("用户名或者密码有误");
		}

		Map<String, Object> paramMap = Maps.newHashMap();
		paramMap.put("userId", systemUser.getId());
		paramMap.put("userType", wechatType);
		// 根据UserId查询已经绑定的公众号微信
		List<Map<String, Object>> wechat = loginMapper.queryBindByMap(paramMap);
		if (null != wechat && wechat.size() > 0) {
			return Result.error("当前用户已经绑定微信号！");
		}

		// 写入绑定关系
		Map<String, Object> param = new HashMap<>();
		param.put("userId", systemUser.getId());
		param.put("openId", openId);
		param.put("systemId", 0);
		// 0小程序 1 公众号
		param.put("userType", wechatType);

		loginMapper.insertBindRef(param);

		// 登录当前用户
		return this.doLogin(systemUser, null);
	}


	@Override
	@Transactional(rollbackFor = Exception.class)
	public Object unbindUser(Integer userId) {
		Map<String, Object> param = Maps.newHashMap();
		param.put("userId", userId);
		return loginMapper.delBindInfo(param);
	}


	@Override
	public Result judgeWeChatIsBind(Integer userId) {
		JSONObject param = new JSONObject();
		param.put("userId", userId);
		param.put("systemId", 0);
		int result = loginMapper.judgeWeChatIsBind(param);
		if (result <= 0) {
			return Result.error("该账号未绑定微信");
		}
		return Result.ok();
	}
	
	
}
